<?php namespace Colin\TagManager;

use Illuminate\Support\ServiceProvider;

class TagManagerServiceProvider extends ServiceProvider {

    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Bootstrap the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->package('colin/tag-manager');
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('tags', function($app, $payload){
            $label = array_get($payload, 0);
            if (0 >= strlen($label)) throw new \Exception('label 長度不可為0');

            return new Manager(array_get($payload, 0));
        });
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return array('tags');
    }

}
