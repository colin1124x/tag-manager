<?php namespace Colin\TagManager;

use Tag;

class Manager
{
    /** @var string $label 標籤分類 */
    private $label;
    public function __construct($label)
    {
        $this->label = $label;
    }

    /**
     * 取得特定群組所有 tag
     * @return array tags
     */
    public function all()
    {
        return Tag::where('item_table', $this->label)
            ->groupBy('name')
            ->get()->fetch('name')
            ->toArray();
    }

    /**
     * 依傳入 tag 查詢 items
     * @param null|string|array $tag
     * @param int $limit
     * @param int $offset
     * @return array 有包含查詢 tag 的 item_id 陣列
     */
    public function search($tag = null, $limit = 100, $offset = 0)
    {
        $limit = (int) $limit;
        $offset = (int) $offset;
        $item_table = $this->label;
        $tags = $this->parse($tag);
        return Tag::where(function($query) use($item_table, $tags, $limit, $offset) {

            $query->where('item_table', $item_table);
            ! empty($tags) and $query->whereIn('name', $tags);
            (0 < $limit) and $query->limit($limit)->offset($offset);

        })->get()->fetch('item_id')->toArray();
    }

    /**
     * 取得 item 上所有標籤
     * @param int $item_id
     * @return array 標籤陣列
     */
    public function tags($item_id)
    {
        return Tag::where($this->buildConditions($item_id))->get()->fetch('name')->toArray();
    }

    /**
     * 替 item 打上標籤
     * @param int $item_id
     * @param string|array $tag
     * @return array 目前此 item 所有標籤
     */
    public function tag($item_id, $tag)
    {
        $tags = array_diff($this->parse($tag), $this->tags($item_id));
        foreach ($tags as $tag) {
            with(new Tag(array(
                'item_table' => $this->label,
                'item_id' => (int)$item_id,
                'name' => $tag)))->save();
        }
        return $this->tags($item_id);
    }

    /**
     * 移除 item 指定標籤
     * @param int $item_id
     * @param string|array $tag
     * @return bool
     */
    public function untag($item_id, $tag)
    {
        $tags = $this->parse($tag);

        if ( ! empty($tags)) {
            Tag::where($this->buildConditions($item_id))->whereIn('name', $tags)->delete();
        }

        return $this->tags($item_id);
    }

    /**
     * 清除指定 item 所有資料庫紀錄
     * @param $item_id
     * @return bool
     */
    public function purge($item_id)
    {
        return Tag::where('item_id', $item_id)->delete();
    }

    /**
     * 遞迴解析 tag
     * @param string|array $tag
     * @return array 標籤陣列
     */
    public function parse($tag)
    {
        $me = $this;
        if (is_array($tag)) return array_unique(array_reduce($tag, function($carry, $item) use($me) {
            return array_merge($carry, $me->parse($item));
        }, array()));

        $tag = trim($tag);

        return $tag ? preg_split('/\s+/', $tag) : array();
    }

    private function buildConditions($item_id)
    {
        // todo encode table name
        $item_table = $this->label;
        return function($query) use($item_table, $item_id) {
            $query->where('item_table', $item_table)
                ->where('item_id', (int)$item_id);
        };
    }
}
