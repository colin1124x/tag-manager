<?php

class TagManagerTest extends TagManagerTestCase
{
    public function testParse()
    {
        $tag = 'a b';
        
        $this->assertEquals(array('a', 'b'), $this->tag->parse($tag));

        $tag = array('c', $tag);

        $this->assertEquals(array('c', 'a', 'b'), $this->tag->parse($tag));
    }

    public function testTag()
    {
        $this->tag->tag(1, '');
        $this->tag->tag(1, array());
        $this->assertEquals(0, count($this->tag->tags(1)));

        $this->tag->tag(1, 'b a');
        $this->assertTagsEquals(array('b', 'a'), $this->tag->tags(1));

        $this->tag->tag(2, 'b');
        $this->assertTagsEquals(array('a', 'b'), $this->tag->all());

        $this->tag->tag(2, 'c');
        $this->assertTagsEquals(array('a', 'b', 'c'), $this->tag->all());

        $this->assertTagsEquals(array(1, 2), $this->tag->search('a c'));

        $this->tag->untag(2, 'b c');
        $this->assertEmpty($this->tag->tags(2));

        $this->tag->purge(1);
        $this->assertEmpty($this->tag->tags(1));
    }

    public function testTagDuplicate()
    {
        $this->tag->tag(1, 'b a');
        $this->tag->tag(1, 'b a');

        $this->assertTagsEquals(array('b', 'a'), $this->tag->tags(1));
    }
}
