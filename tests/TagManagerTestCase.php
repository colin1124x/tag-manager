<?php

class TagManagerTestCase extends Orchestra\Testbench\TestCase
{
    protected static $_tag;
    /** @var Colin\TagManager\Manager */
    protected $tag;

    public function setUp()
    {
        parent::setUp();

        $artisan = $this->app['artisan'];
        $artisan->call('migrate', array(
            '--database' => 'test',
            '--path' => '../src/migrations',
        ));

        if ( ! isset(self::$_tag)) {
            self::$_tag = $this->app->make('tags', array('test'));
        }

        if ( ! isset($this->tag)) {
            $this->tag = self::$_tag;
        }
    }

    /**
     * Define environment setup.
     *
     * @param  \Illuminate\Foundation\Application  $app
     * @return void
     */
    protected function getEnvironmentSetUp($app)
    {
        $app['path.base'] = __DIR__ . '/../src';
        $app['config']->set('database.default', 'test');
        $app['config']->set('database.connections.test', array(
            'driver' => 'sqlite',
            'database' => ':memory:',
            'prefix' => '',
        ));

        $app->register(new Colin\TagManager\TagManagerServiceProvider($app));
    }

    protected function assertTagsEquals($expected, $actual, $msg = null)
    {
        sort($expected);
        sort($actual);
        $this->assertEquals($expected, $actual, $msg);
    }

}
