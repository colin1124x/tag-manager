# colin/tag-manager
標籤系統 - depends on [laravel 4.1.*]

---

## 直接建構 

```php

use Colin\TagManager\Manager;

// $label 可以當作對應資料所屬資料表名
$tags = new Manager($label);

```

## 從 Ioc 容器取得

```php

$tags = App::make('tags', [$label]);

```

## Methods

```php

// 用 tag 搜尋 item_id 陣列
$tags->search($tags); // return item_id array

// 用 item_id 找所有 tags
$tags->tags($item_id); // return tags array

// 替 item_id 加標籤
$tags->tag($item_id, $tags); // return tags array

// 移除 item_id 特定標籤
$tags->untag($item_id, $tags); // return tags array

// 清除 item_id 所有標籤
$tags->purge($item_id); // boolean


```

[laravel 4.1.*]:http://laravel.tw/docs/4.2